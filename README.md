##  KindEditor 简介

KindEditor 是一个轻量级, 开源(LGPL), 跨浏览器, 基于web的所见即所得html编辑器. KindEditor 给textarea 添加了富文本编辑的能力. 
From：netmou

## 官方网址 [不再维护]

http://kindeditor.org/

## 与原版的不同
* 修正IE11下弹窗位置偏移bug v4.1.12_fix
* 将视频代码替换成vedio v4.1.13
* 随着flash生命周期结束，移除flash相关代码 上传部分移植了Nkeditor的webuploader代码 v4.1.43
* 允许从word中粘贴本地图片（只能单个单个的粘贴图片，行为和ueditor类似）